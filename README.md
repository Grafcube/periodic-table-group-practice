# Periodic Table Group Practice

My friend asked me to make this and I finished it in 5 hours. I'll probably never touch this again.

I got the periodic table CSV file [here](https://gist.github.com/GoodmanSciences/c2dd862cd38f21b0ad36b8f96b4bf1ee#file-periodic-table-of-elements-csv-L12) except I removed the *f*-block elements.
