﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace PeriodicTableGroupPractice
{
	class Program
	{
		static List<List<string>> PeriodicTable;

		static void Main(string[] args)
		{
			string tablePath = Path.Combine(Directory.GetCurrentDirectory(), @"periodic_table_of_elements.csv");
			PeriodicTable = ParsePeriodicTable(tablePath);

			PrintHeading();
			Console.WriteLine("Type `help` to see available commands");
			Console.WriteLine();
			while (true)
			{
				var command = ReadCommands();
				switch (command[0])
				{
					case "help":
						Command_help();
						break;
					case "view":
						if (command.GetLength(0) == 1)
						{
							Console.WriteLine("view [int x]");
							Console.WriteLine("\t DESCRIPTION");
							Console.WriteLine("\t\t View the elements in [x] group.");
							Console.WriteLine("\t EXAMPLE");
							Console.WriteLine("\t\t view 13");
						}
						else
						{
							Command_view(command.Skip(1).ToArray());
						}
						break;
					case "ask":
						if (command.GetLength(0) == 1)
						{
							Console.WriteLine("ask [int x]");
							Console.WriteLine("\t DESCRIPTION");
							Console.WriteLine("\t\t Answer for the elements in [x] group.");
							Console.WriteLine("\t EXAMPLE");
							Console.WriteLine("\t\t ask 15");
						}
						else
						{
							Command_ask(command.Skip(1).ToArray());
						}
						break;
					case "start":
						Command_start();
						break;
					default:
						Console.WriteLine("Invalid command. Type `help` to see available commands.");
						break;
				}
			}
		}

		static void PrintHeading()
		{
			Console.WriteLine("Grafcube's Periodic Table Group Practice App");
			Console.WriteLine("2020");
		}

		static string[] ReadCommands()
		{
			Console.Write(">>> ");
			string[] commands = Console.ReadLine().Split(' ');
			return commands;
		}

		static void Command_help()
		{
			Console.WriteLine("view [int x]");
			Console.WriteLine("\t DESCRIPTION");
			Console.WriteLine("\t\t View the elements in [x] group.");
			Console.WriteLine("\t EXAMPLE");
			Console.WriteLine("\t\t view 13");
			Console.WriteLine();
			Console.WriteLine("ask [int x]");
			Console.WriteLine("\t DESCRIPTION");
			Console.WriteLine("\t\t Answer for the elements in [x] group.");
			Console.WriteLine("\t EXAMPLE");
			Console.WriteLine("\t\t ask 15");
			Console.WriteLine();
			Console.WriteLine("start");
			Console.WriteLine("\t DESCRIPTION");
			Console.WriteLine("\t\t Start randomly answering the given group.");
			Console.WriteLine("\t\t Type `end` to stop.");
		}

		static void Command_view(string[] vs)
		{
			int group;
			bool isInt = int.TryParse(vs[0], out group);
			if (isInt)
			{
				var groupElements = GetElementsInGroup(group);
				Console.WriteLine(String.Join(", ", groupElements));
			}
			else
			{
				Console.WriteLine("`view` requires an integer value. Type `help` to see available commands.");
			}
		}

		static bool Command_ask(string[] vs)
		{
			int group;
			bool isInt = int.TryParse(vs[0], out group);
			if (isInt)
			{
				var groupElements = GetElementsInGroup(group);
				if (groupElements.GetLength(0) != 1)
				{
					Console.WriteLine("Separate each element with a comma. No whitespace.");
					Console.WriteLine("What are the elements in Group " + vs[0] + "?");
					Console.Write("--> ");
					var input = Console.ReadLine();
					if (input.StartsWith("end") || input.StartsWith("end "))
					{
						return false;
					}
					var answers = input.Split(',');
					Console.WriteLine("ORIGINAL --> " + String.Join(", ", groupElements));
					Console.WriteLine("ANSWERS  --> " + String.Join(", ", answers));
					//Console.WriteLine("WRONG ANSWERS  --> " + String.Join(", ", answers.Except(groupElements, StringComparer.OrdinalIgnoreCase)));
					//Console.WriteLine("MISSED ANSWERS --> " + String.Join(", ", groupElements.Except(answers, StringComparer.OrdinalIgnoreCase)));
					return true;
				}
				else
				{
					Console.WriteLine("Group number must be in 1 to 18.");
					return false;
				}
			}
			else
			{
				Console.WriteLine("`ask` requires an integer value. Type `help` to see available commands.");
				return false;
			}
		}

		static void Command_start()
		{
			Console.WriteLine("Type `end` to stop.");
			var rand = new Random();
			int grp = new int();
			bool cont = true;
			while (true)
			{
				if (cont)
				{
					grp = rand.Next(1, 3) == 1 ? rand.Next(1, 3) : rand.Next(13, 19);
					cont = Command_ask(new string[] { grp.ToString() });
				}
				else
				{
					break;
				}
			}
		}

		static string[] GetElementsInGroup(int grp)
		{
			List<string> elementsInGroup = new List<string>();
			if (grp >= 1 && grp <= 18)
			{
				foreach (var element in PeriodicTable)
				{
					if (int.Parse(element[8]) == grp)
					{
						elementsInGroup.Add(element[1]);
					}
				}
				return elementsInGroup.ToArray();
			}
			else
			{
				return new string[1] { "Group number must be in 1 to 18." };
			}
		}

		static List<List<string>> ParsePeriodicTable(string csv_path)
		{
			List<string> ptablelist = File.ReadAllLines(csv_path).Skip(1).ToList();
			List<List<string>> ptable = new List<List<string>>();
			foreach (var line in ptablelist)
			{
				ptable.Add(line.Split(',').ToList());
			}
			return ptable;
		}
	}
}
